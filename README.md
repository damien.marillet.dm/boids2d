Modelisation of 2D Boids behaviours.

Inspired by : 
"Coding Adventure: Boids" from Sebastian Lague
https://www.youtube.com/watch?v=bqtqltqcQhw&t=406s


How to use it:

If it is not the case already, from the project menu go to Assets/Scenes/ and double click on the only scene called SampleScene.unity

To modify the simulation parameters, from the project menu go to Assets/ and click on the Bond prefab. 
Now in the Inspector menu all the parameters to modify the boids behaviour can be adjusted manually in the Boid Movement Script attached to the prefab.
Whereas the total number boids can be adjusted from the Inspector menu after selecting the Boids Initializer GameObject in the scene.

Possible extensions:
- Make the Field of view visible.
- Different species with pack behaviours.
- Obstacle avoidance.
- Fleeing and pursuit behaviours: a modelisation of preys and predators.


Demo gif from the project:

![image alternative text](/Demo.gif)
