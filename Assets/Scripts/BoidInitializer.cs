﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoidInitializer : MonoBehaviour
{
    public int nbBoids;
    public GameObject boidPrefab;
    public Camera cam;
    
    void Start()
    {
        for(int i=0; i<nbBoids; i++)
        {
            int randomWidth=Random.Range(1, Screen.width);
            int randomHeight = Random.Range(1, Screen.height);
            Vector3 randomPos = cam.ScreenToWorldPoint(new Vector3(randomWidth, randomHeight, 0));
            float randAngle = Random.Range(0f, 360f);
            Quaternion randomRotation = Quaternion.Euler(0f,0f, randAngle);
            GameObject boid = Instantiate(boidPrefab, randomPos, randomRotation);
        }
    }
}
