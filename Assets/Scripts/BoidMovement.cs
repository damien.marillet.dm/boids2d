﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoidMovement : MonoBehaviour
{
    public float m_Speed;
    public float fieldOfViewAngleRange;
    public bool separationEnabled, cohesionEnabled, alignmentEnabled;
    [Range(Single.Epsilon,4f)]
    public float separationWeight,cohesionWeight, alignmentWeight;
    public float separationRadius, cohesionRadius, alignmentRadius;

    /*private GameObject parametersGameObject;
    private Parameters parameters;
    */
    
    private Camera cam;
    private Rigidbody2D rb;
    private Collider2D col;
    private Vector2 mousePos;
    private int pixelGap =0;

    private float fovAngle;
    private float sWeight, cWeight, aWeight; // different de 0 !!!
    private float sRadius, cRadius, aRadius;
    private void Awake()
    {
        cam = Camera.main;
    }
    void Start()
    {
        initColor();
        rb = GetComponent<Rigidbody2D>();
        col  = GetComponent<Collider2D>();
    }
    private void Update()
    {
        AdjustSettings();
        mousePos = cam.ScreenToWorldPoint(Input.mousePosition);
        UpdateBoidPosition();
    }
    void FixedUpdate()
    {
        Vector3 lookDir = transform.up;
        ApplyBehaviours(ref lookDir);

        float angle = Mathf.Atan2(lookDir.y, lookDir.x) * Mathf.Rad2Deg - 90f;
        rb.rotation = angle;
        rb.MovePosition( transform.position + transform.up.normalized  * Time.fixedDeltaTime * m_Speed); 
    }

 
    /*private void OnDrawGizmos()
    {
        float radiusMax = Mathf.Max(cRadius, Mathf.Max(aRadius, sRadius));
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, radiusMax);

        Gizmos.color = Color.blue;
        Vector3 dir1 = Quaternion.AngleAxis(fovAngle, Vector3.forward) * transform.up.normalized * radiusMax;
        Vector3 dir2 = Quaternion.AngleAxis(-fovAngle,Vector3.forward) * transform.up.normalized * radiusMax;
        Gizmos.DrawRay(transform.position, dir1);
        Gizmos.DrawRay(transform.position, dir2);

        Gizmos.color = Color.green;
        Gizmos.DrawRay(transform.position , transform.up);

    }*/
    private void ApplyBehaviours(ref Vector3 lookDir)
    {
        if (separationEnabled)
            lookDir += SeparationSteering().normalized * sWeight;
        if (cohesionEnabled)
            lookDir += CohesionSteering().normalized * cWeight;
        if (alignmentEnabled)
            lookDir += AlignmentSteering().normalized * aWeight;

    }

    private void AdjustSettings() {
        sWeight = Mathf.Clamp(separationWeight, 0.0001f, Mathf.Infinity);
        cWeight = Mathf.Clamp(cohesionWeight, 0.0001f, Mathf.Infinity);
        aWeight = Mathf.Clamp(alignmentWeight, 0.0001f, Mathf.Infinity);

        sRadius = Mathf.Clamp(separationRadius,0f, Mathf.Infinity);
        cRadius = Mathf.Clamp(cohesionRadius, 0f, Mathf.Infinity);
        aRadius = Mathf.Clamp(alignmentRadius, 0f, Mathf.Infinity);

        fovAngle = Mathf.Clamp(fieldOfViewAngleRange, 0f, 180f);
    }
    private bool IsInFoV( Vector3 targetPos, float maxAngle) //pour l'instant en fct du transform.position du boid en collision ( et pas exactement le point de contact)
    {
        Vector3 lookDirection = (targetPos - transform.position).normalized;
        float angle = Vector3.Angle(transform.position, lookDirection) ;
        return (angle<=maxAngle);
    }
    
    private Vector3 SeparationSteering()
    {
        List<Collider2D> colList = new List<Collider2D>();
        ContactFilter2D filter = new ContactFilter2D();
        int cpt = Physics2D.OverlapCircle(transform.position,sRadius, filter.NoFilter(), colList);
        
        Vector3 steeringSum = Vector3.zero;
        foreach ( Collider2D col in colList)
        {
            if (col.gameObject.Equals(this.gameObject)) { continue; }

            if(!IsInFoV(col.transform.position,fovAngle)) { continue; }

            Vector3 targetPos = col.transform.position;
            Debug.DrawLine(transform.position, col.transform.position, Color.yellow, 0.0f);
            Vector3 repulsiveForce = (transform.position - targetPos).normalized / (transform.position-targetPos).magnitude;

            steeringSum += repulsiveForce;
            
        }
        return steeringSum;
    }
    private Vector3 AlignmentSteering()
    {
        List<Collider2D> colList = new List<Collider2D>();
        ContactFilter2D filter = new ContactFilter2D();
        int cpt = Physics2D.OverlapCircle(transform.position, aRadius, filter.NoFilter(), colList);

        Vector3 alignmentSum = Vector3.zero;
        foreach (Collider2D col in colList)
        {
            if (col.gameObject.Equals(this.gameObject)) { continue; }

            alignmentSum += col.transform.up;
        }

        return alignmentSum;
    }

    private Vector3 CohesionSteering() {

        List<Collider2D> colList = new List<Collider2D>();
        ContactFilter2D filter = new ContactFilter2D();
        int cpt = Physics2D.OverlapCircle(transform.position, cRadius, filter.NoFilter(), colList);

        Vector3 cohesionCenter = Vector3.zero;

        foreach (Collider2D col in colList)
        {
            if (col.gameObject.Equals(this.gameObject)) { continue; }

            Vector3 targetPos = col.transform.position;
            cohesionCenter += targetPos - transform.position;

        }
        return cohesionCenter ;
    }
    private void LookAtMouse()
    {
        Vector2 lookDir = mousePos - rb.position;
        float angle = Mathf.Atan2(lookDir.y, lookDir.x) * Mathf.Rad2Deg - 90f;
        rb.rotation = angle;
    }
    private void UpdateBoidPosition()
    {
        Vector3 scrPos = cam.WorldToScreenPoint(transform.position);
        if (scrPos.x < 0) TeleportRight(scrPos);
        if (scrPos.x > Screen.width) TeleportLeft(scrPos);
        if (scrPos.y < 0) TeleportTop(scrPos);
        if (scrPos.y > Screen.height) TeleportBottom(scrPos);
    }
    
    private void TeleportBottom(Vector3 scrPos)
    {
        Vector3 goalScrPos = new Vector3(scrPos.x, pixelGap, scrPos.z);
        Vector3 goalWorldPos = cam.ScreenToWorldPoint(goalScrPos);
        transform.position = goalWorldPos;

    }

    private void TeleportTop(Vector3 scrPos)
    {
        Vector3 goalScrPos = new Vector3(scrPos.x, Screen.height- pixelGap, scrPos.z);
        Vector3 goalWorldPos = cam.ScreenToWorldPoint(goalScrPos);
        transform.position = goalWorldPos;
    }

    private void TeleportLeft(Vector3 scrPos)
    {
        Vector3 goalScrPos = new Vector3(pixelGap, scrPos.y, scrPos.z);
        Vector3 goalWorldPos = cam.ScreenToWorldPoint(goalScrPos);
        transform.position = goalWorldPos;
    }

    private void TeleportRight(Vector3 scrPos)
    {
        Vector3 goalScrPos = new Vector3( Screen.width- pixelGap, scrPos.y, scrPos.z);
        Vector3 goalWorldPos = cam.ScreenToWorldPoint(goalScrPos);
        transform.position = goalWorldPos;
    }

    private void ChangeColor()
    {
        Color color = new Color(r: UnityEngine.Random.Range(0f, 255f), g: UnityEngine.Random.Range(0f, 255f), b: UnityEngine.Random.Range(0f, 255f));
        this.GetComponent<SpriteRenderer>().color = color;
    }

    private void initColor()
    {
        Color startColor = new Color(r: UnityEngine.Random.Range(0f, 1f), g: UnityEngine.Random.Range(0f,1f), b: 1f, a:1f);
        SpriteRenderer sRenderer = this.GetComponent<SpriteRenderer>();
        sRenderer.color = startColor;
        //sRenderer.color = globalColor;
    }

    private void OnMouseDown()
    {
        ChangeColor();
    }

    private void DrawFieldOfView()
    {
        throw new NotImplementedException();
    }

}
