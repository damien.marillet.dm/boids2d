﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parameters : MonoBehaviour
{
    public bool separation, cohesion, alignment;
    public float separationRadius, cohesionRadius, alignmentRadius;
    public float separationAngle, cohesionAngle, alignmentAngle; //IMPLENTER VERIFICATION D'ANGLES ( calculs d'angle avc col.transform et un if )
    public float separationWeight, cohesionWeight, alignmentWeight;
}
